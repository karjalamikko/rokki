import { OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { QuizService } from '../../quiz.service';


export class HomePage implements OnInit{


// tslint:disable-next-line: no-shadowed-variable
constructor(public Router: Router,
            public activatedRoute: ActivatedRoute,
            private quizService: QuizService) { }

private duration: number;
private durationSeconds: number;

ngOnInit() {
    this.duration = Number(this.activatedRoute.snapshot.paramMap.get('duration'));
    this.durationSeconds = Math.round((this.duration) / 1000);
}

private goHome() {
    this.quizService.initialize();
    this.Router.navigateByUrl('home');
}

}

