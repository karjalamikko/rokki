import { Component, OnInit } from '@angular/core';
import { QuizService } from '../quiz.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{


  private feedback: string;
  private duration: number;

  constructor(public router: Router, private quizService: QuizService) {}


  ngOnInit() {
    this.quizService.addQuestions();
    this.quizService.initialize();
    this.feedback = '';
  }

  private checkOption(option: number) {
    this.quizService.setOptionSelected();
    if (this.quizService.areAllOptionsUsed() ) {
      this.quizService.setQuestion();
    }
    else {
      if (this.quizService.isCorrectOption(option)) {
        this.feedback =
        this.quizService.getCorrectOptionOfActiveQuestion() +
        ' Oikea vastaus! Jatka eteenpäin!';
      }
      else {
        this.feedback = 'Väärin! Arvauksia jäljellä: ' +
        (this.quizService.getNumberOfOptionsOfActiveQuestion() -
        this.quizService.getIndexOfOptionCounter());
      }
    }
  }

  private continue() {
    if (this.quizService.isFinished()) {
      this.duration = this.quizService.getDuration();
      this.router.navigateByUrl('result/' + this.duration);
    }
    else {
      this.quizService.setQuestion();
    }
  }

}
