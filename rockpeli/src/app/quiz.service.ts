import { Injectable } from '@angular/core';
import { Kysymykset } from '../kysymykset';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

private questions: Kysymykset[] = [];
private activeQuestion: Kysymykset;
private isCorrect: boolean;
private isSelected: boolean;
private questionCounter: number;
private optionCounter: number;
private startTime: Date;
private endTime: Date;
private duration: number;


  constructor() {}

  public addQuestions() {
    this.questions = [{
      quote: 'Kun mä sinut kohtasin',
      options: [
        'Oli tähdet kirkkaat niin',
        'Oli ilta ihanin',
        'Olit kaikista kaunehin'],
      correctOption: 2
    },
    {
    quote: 'Mul on taskussa kakskytä senttiä vain',
      options: [
        'Siinä on kaikki sen luojalta sain',
        'Ei taskut kovinkaan syvät ole',
        'Oisko sul vippaa vielä kahdeksänkymmentä'],
      correctOption: 1
    },
    {
      quote: 'Hetki lyö, viime hetki lyö',
        options: [
          'Silloin täyttyi yö',
          'Kukaan aikaa lahjomaan ei käydä voi milloinkaan',
          'Milloin sut mä nähdä saan'],
        correctOption: 2
      },
      {
        quote: 'Silloin satulinna kaukomaan',
          options: [
            'Oismme yhdessä vain',
            'Ja aamu taas sarastais',
            'Meille kuuluis kokonaan'],
          correctOption: 3
        },
        {
          quote: 'Huone sataviis, Saavu sinne pliis',
            options: [
              'Tuo samalla vessapaperia',
              'Paapaadiidappappaa...',
              'Katsottas vaan telkkaria'],
            correctOption: 1
          }
  ];
  }

  public setQuestion() {
    this.optionCounter = 0;
    this.isCorrect = false;
    this.isSelected = false;
    this.activeQuestion = this.questions[this.questionCounter];
    this.questionCounter++;
  }


  public initialize() {
    this.questionCounter = 0;
    this.startTime = new Date();
    this.setQuestion();
  }

  public getQuestions(): Kysymykset[] {
    return this.questions;
  }

  public getActiveQuestion(): Kysymykset {
    return this.activeQuestion;
  }

  public getQuoteOfActiveQuestion(): string {
    return this.activeQuestion.quote;
  }

  public getOptionsOfActiveQuestion(): string[] {
    return this.activeQuestion.options;
  }

  public getIndexOfActiveQuestion(): number {
    return this.questionCounter;
  }

  public getNumberOfQuestions(): number {
    return this.questions.length;
  }

  public getNumberOfOptionsOfActiveQuestion(): number {
    return this.activeQuestion.options.length;
  }

  public getIndexOfOptionCounter(): number {
    return this.optionCounter;
  }

  public getCorrectOptionOfActiveQuestion(): string {
    return this.activeQuestion.options[this.activeQuestion.correctOption];
  }

  public setOptionSelected() {
    this.isSelected = true;
  }

  public isOptionSelected(): boolean {
    return this.isSelected;
  }

  public areAllOptionsUsed(): boolean {
    this.optionCounter++;
    return (this.optionCounter > this.activeQuestion.options.length) ? true : false;
  }

  public isCorrectOption(option: number): boolean {
    this.isCorrect = (option === this.activeQuestion.correctOption) ? true : false;
    return this.isCorrect;
  }

  public isAnswerCorrect(): boolean {
    return this.isCorrect;
  }

  public isFinished(): boolean {
    return (this.questionCounter === this.questions.length) ? true : false;
  }

  public getDuration(): number {
    this.endTime = new Date();
    this.duration = this.endTime.getTime() - this.startTime.getTime();
    return this.duration;
  }

}
